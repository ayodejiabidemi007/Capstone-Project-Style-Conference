![](https://img.shields.io/badge/Microverse-blueviolet)

# HTML/CSS & JavaScript capstone project - Conference Page

> This project is a final capstone project for HTML/CSS & JavaScript. In this capstone project I built an online website for a conference using Cindy's design on Behance, but I personalized the content.  

## Built With

- HTML, CSS & JAVASCRIPT.

## Live Demo    

[Live Site Demo](https://demix007.github.io/Capstone-Project-Style-Conference/)

[Project Description Video](https://www.loom.com/share/e24d0cb3af1d4675a9372fed7b3c3eb2)

## Getting Started

- To get a local copy up and running follow these simple example steps:

### Setup

- Download or clone this Repo in your local machine.

### Usage

- Open the index.html in your browser

👤 **Ayodeji Abidemi**

- GitHub: [@githubhandle](https://github.com/demix007)
- Twitter: [@twitterhandle](https://twitter.com/dat_dope_demix)
- LinkedIn: [LinkedIn](https://linkedin.com/in/ayodeji-abidemi-b76935218/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://github.com/demix007/Capstone-Project-Style-Conference/issues).

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Inspiration for this design was from Cindy Shin in Behance as the design is her original idea. 

## 📝 License

This project is [MIT](./LICENSE) licensed.

_NOTE: we recommend using the [MIT license](https://choosealicense.com/licenses/mit/) - you can set it up quickly by [using templates available on GitHub](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-license-to-a-repository). You can also use [any other license](https://choosealicense.com/licenses/) if you wish._
